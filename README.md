Rewards Network Encryption – Java Examples
==========================================

Overview
--------

This repository provides examples of encrypting and then decrypting values using the cryptographic configurations
Rewards Network supports: that is, RSA-OAEP with a SHA-256 hash function and mask generation function for padding using
either SHA-1 or SHA-256.

For the Rewards Network Test/Staging and Production environments, **only** the X.509 certificate/RSA public key
is provided.

Prerequisites
-------------

* A Java 8 Java Development Kit (JDK) or later
* Apache Maven 3 or later

Usage
-----

### Running with No Added Encrypted Values

```shell
mvn compile exec:java
```

### Running with Additional Encrypted Values

This helps validate that the encryption code and configuration is written properly for integration with Rewards Network. Refer to the [Credit Card Encryption Apigee Portal](https://rewardsnetwork-portal.apigee.io/credit-card-encryption) page for current public key values to use for encrypting cards.

* X.509 certificate: `./src/main/resources/cert.crt`
* RSA public key: `./src/main/resources/public.crt`

```shell
export ENCRYPTED_VALUE_1='example_1'
export ENCRYPTED_VALUE_2='example_2'
mvn compile exec:java -Dexec.args="${ENCRYPTED_VALUE_1} ${ENCRYPTED_VALUE_2}"
```
