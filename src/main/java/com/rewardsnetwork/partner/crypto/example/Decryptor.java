package com.rewardsnetwork.partner.crypto.example;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.MGF1ParameterSpec;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Decryptor {
    private static final List<MGF1ParameterSpec> SUPPORTED_MGF1_PARAMETER_SPECS =
            Collections.unmodifiableList(Arrays.asList(MGF1ParameterSpec.SHA1, MGF1ParameterSpec.SHA256));

    private final PrivateKey key;

    public Decryptor(PrivateKey key) {
        this.key = key;
    }

    public String decrypt(String encrypted) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        BadPaddingException lastThrown = null;

        for (MGF1ParameterSpec mgf1ParameterSpec : SUPPORTED_MGF1_PARAMETER_SPECS) {
            try {
                Cipher oaepFromInit = Cipher.getInstance("RSA/ECB/OAEPPadding");
                OAEPParameterSpec oaepParams =
                        new OAEPParameterSpec("SHA-256", "MGF1", mgf1ParameterSpec,
                                PSource.PSpecified.DEFAULT);
                oaepFromInit.init(Cipher.DECRYPT_MODE, key, oaepParams);
                byte[] unencoded = Base64.decodeBase64(encrypted);
                byte[] pt = oaepFromInit.doFinal(unencoded);
                return new String(pt, StandardCharsets.UTF_8);
            } catch (BadPaddingException currentThrown) {
                lastThrown = currentThrown;
            }
        }

        if (lastThrown != null) {
            throw lastThrown;
        }

        throw new RuntimeException("No supported MGF1ParameterSpecs have been configured!");
    }
}
