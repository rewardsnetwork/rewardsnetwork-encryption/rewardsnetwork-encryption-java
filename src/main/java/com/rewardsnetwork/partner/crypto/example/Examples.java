package com.rewardsnetwork.partner.crypto.example;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Examples {
    /**
     * This is a list of known good encrypted values, given the public key, using both SHA-1 and SHA-256 MGF1
     * parameter specifications.
     */
    private static final List<Preencrypted> PREENCRYPTED_VALUES = Collections.unmodifiableList(Arrays.asList(
            new Preencrypted("open sesame",
                    "u2IS4nLAE3kWAIJ1db1rBJAuKhTKpZCwebQD3O7ZKW/PB58YZrA/TfGWIaD7B8RZvSwZdVbQc2s7GZVik47bb98WGq8NpJRBxVa5rZPZ8MqnT1NK+E+fsGvpDa+zYB4LKh9DFa2PYbk2/e2ULu576pg2jNUFQyxA7K/d1e7zNvNFVvjqPSuz/bj0Sv7i5ow01Y53qzSRhxMr0fb6nlQGbAgAUo0oHLIN2D/9LdqMQTz0NN6mUuNYpVTGIVcVyru3GNfH5gX5kLLTCHBJzj56SpDkeIiBkYz9UuhakWpIpKd35+p7ah02C4VIWDFV0BK2Jq0I9aifwveFBIa73XTCEzl8LfR0oABX4R2aUmG5VqUz16dMSYTrprsDTTespXJkEDN/nWOsHECM+JD/MO1jz5TtTDhxP/RSc22qr9Gi/JoLhTFUuNj7fmbpl1hLAexaqYb9eS1owY/bXwN7neyvwfVZerWQ8GMVw/RKNzXgE3QwaajS7H6aNc6wRMPy1CthQ8cdDSpPA60JUeUiTy7frvWHPNDczECTb2z7AP8Qmn/tbdaCjpJNfMkXijvZnR5iFOm1XHj+d4TGrt49M2bWNFALi5siAcqhIv/o1PwltIGePOfnJf0GNGyS4xFclpkt7SmAt/tFgO69l7UwYsUcRd7Askb2Egz2d06i62eA0+0=",
                    "giAaLI1Ndxj7mCdKVgOz6m3zt1h/TLNurUW8S+EqVArY2R5obs/yAiR/VgYkYZ5pL8JMN8IyKYT8VSSN0NYdLZ5ItqtZKSRn1AnoeM3L7t0U8oJ70aZLWxOSQlcVqDxpQs/pGZg+R6Uqd9PXkFwgZukQkdrxC3PYb+zxKkWJrfBEDLU0fmYHR3YudkhyzA70tumBiu9r+lrCGJPy6b3pK2/AvluPp31ylxXagUin1UgpRFHYGiil5hRPQ+l1KKfFhAUKKr6Ea/S5RUsTXtxTALLS4DFGes7JPDrGFtZ4kQoV16nN+e5vTM5QDr/dcGixHc3z06t77xhqGOQu8OOeZeZMSp4ORgzc83RaX87oz5pzHgg/3Gj0iMXep8v9SvLbtArAxslFL4Tf8Azvi6H6RW5x71KYt+iFHYgGGTCuj3Vyaf7ZzO1AorqzazxZD87npfB28MmZjjKbalrFC5UI1EH8f033b71C+OxDvnhNCcY98TvwTHjYrXzjYurpAX9KgmQwwqOusdQlv6DDRnfTVmum2yIydAx/cb19SKd2fYq5rmAFpvoYZrBZ/rdS1UI27LEX2D/dNgaM6tzJxkJPLD66/S14Ye9syCVlTSSAzbuwlmsKiZpayXNy7N1Mi58xZ7ZgnamTX1v5i0cO5gh6bTNQxHb1X670n23DGnumzF4="),
            new Preencrypted("The quick brown fox jumped over the lazy dog.",
                    "j/nBKOuYd5UsfCh3ucW661+vqGGC0xOlw3R+dvxIbPRBnQrpEq/50YUTFYqzrUDeDPb/Hw1yfF6SwqPTwNPU03EW6TYu8uwy9Yi1F23weFYGO8hrojIqclJG40D2dM3Vp/1sR+Egx/w6NY10sR4BbwTyySRBX21OX/Iv2jozEq/WHJERTVB4LEqxhDR9BPPNbf0TpVFLcoSOU7S/vYRZb7E+My7MdxqW7LD2syUQQsrlHMbRCkgqQggD7Eav7f78aksrwgFZbjRuE1X1aDSGTlHQI5EQT8GD/QCVAE+EQxqzh9X8qPxW6s7xvtn01H7NxHRmg3vX/qr8KRWH63U4d8Mzx8/h3N2HJVqUbFmdrdbsbfzFnlx164mMk1hBPEkiv8bHDjxxTqDpQncJiz3oNh/oc3ZeOOXc4BPfhGoyQju8u/LL70Q2eMGzOtgTKjMmv1LNigUofVAzIXzb3EnntWfBVEAXN4zhlOwLESHt8NUqMMGRNs3Lo24HVBLt8WdhHajBNVlD1xgfuMojsA4v2hXgE0F9WTdkVs/EeK9NUpPqgH9likQ3Tmg9Lg05vzFGlbotR2aQo9LQsXsA36g4GYFS6oqgUzG9mYZJ02US1f88qW80wGLUYwXMjdriMqU2sFMunAvwhJGUkCXajbT99fV+vcogEO6e6XdBM4W6XKs=",
                    "Zzy1s8yKyvo1VhDN4WB88w6LR8B8yn2gqWXqp8DMe7/3YUP+/XL/RbDGoRwVqV+Jza0Vr0Wn2GcRKQdFedmFLJ69Vz08MARa5MkxK6Brs8yw6sSZhPRvBfyhygAPa3s6Ti9xtKiEYcTbTewCJbnwC+8GCY+V2QiCfsqZ2Ngh/ZpfcKjL7dhURuwljBSdPaaWuklq6aadouUGKArH1CA3tGyc8JFG0v/3Z+loXFJCMu+hhTzHIEJLo+X+dLPtJnh6GN+j39XMDOowvxtlJQWi+34pLxkK/dCzV1OG7IqkoGqBeGtfaj4Sn630RGPQct4CLQiczBVkQuPBlR8fP0WHTPNCR3Ldm7nyzTj6OrBm4c6F+WzyJ7xTUAmzKLAnsQyrO5c25y0yDG+Wqh3zxAQOIHngyHvvCHSw3eBlsp5/fSnagiYPEY5+0aHDXn0RMEZ7A/Wx6YnhUyAnGtKSjNbES+zDuUAj9YS2zvJ46QR/tO5rTmgUJwLbhFz9aeZfbsnD6reBwQDZdQeESkym7tJAFX3j6QxKURhs9G9iXICqu7jz++h4r2gH8JhYKWgc0G0J4gSjuHpcI3iJOZ14a77S9vN2Qo+J5YgVrK9xGShkov7tMqN3KeWYNadSMg7qSwbXrXT+KX+zqg2orW5ukBED1gHLF+IYuKynHe8i3sL5gpA="),
            new Preencrypted("1234",
                    "JtkXjnyUcSSO0izTFGQkvVfTBUpaNjEUr7SsHrjrN70LEeRT2r2nhzSLlNaYbchx3r6C3ivN3UkmBT4tiXAg5Jc/fjv+8x6LGjlzf2i0CBAdLSOHobCpXWh32qmIHljtEwoONUXEqApfhei+nSlnXHutlPrqwpvCGDmC9cVEaukBGfOVcpoueDRPzOejkuBcKZtXNxiy9J3KpejPpyiEMkECdqKe2C3RQ+fJ3gWO0eoBUvp83hRAVnE+iJsi++8czAVirH1cuoi3A0aNVTv99LCLo3t7LoSXK6XuZ4+zQEYnQcy5im8lXtjLDPZFwcFCKl5IKU8bAg2gMojM+SQrgjkInKOGf66LGh379Sz8V7QDhm2htoJPatrOllF8C3NMcCamx/3DIIRKyRaRx49yETBPMyZxtd2In+BUTQhdUqtZIn9u2hglspBx7r9EKxZIwdTzGxHiTbhh35Z6BWEW1kwfCAC0m12Ncfp4tfz+ugzQXYujN5Z9217YQYlSkDej924WxWWN40XA9zfkWszunT4YofNDcglGm4rjFeQjYUr/mPBlSzJMVCVrJdGIfUr+7iK4mRPNOSm4/PMBiflN0m+4nF4Cl+IqIUIq6i0VUxVcWI5e7YpIGaPAHUQ9zRsV/00PwYVc1bc0cqfOkIRkXXKUvxhAtgOoLOZ+MlNtA98=",
                    "SF8661L9u7pi6h7oqZWNq0mxIR4g76eyjWObW4fJY6onPI4l0GVcE2BtsaYUpSYdWHeR6tNb0EVUgEOzXxG1ilGsJkl+aHOczem7oacFA7LPHdHEoD5BXPSIOFIz05MHSJGB0tVs+wf78K974YbvFd4gMym1m8zb1p953nEzM9q6y7FFkEfU3DA5SH027Vi602mZZeqZyLZVCpnwXJJPNHRoUiJMPC3+OhrfRoHRnAi8+QD2QI4Xbg57jwqVlWoYx2YAq+ret55zSg+XxlnNkbqFIuZG/dAFP4yFKHB3DM2p8dHKWJnDArUkWeUtJ3Td/sfKTKqL/a/l+EaOIfQnSjqVUnPsNI0mpknE1KrnF9+j+p4UINZaUe6e0s7CyJvOcTYbGBabTeIznGfrP93uVIbDU6o5azRzcmNBRs9zlFNglM+mRIABbI+dgdnF1qsvvWgfehQDebj6r/xp4WWA7NraFPmi4YSKWBuVnrY7Yw0RFtHHNiGrydvWny/5PrvsY6bobzpjzmPrU/McgQla3uKywtiXEnGMkiUlZCGfzPg0OevrWsgdCqcq0RSRXWhKICoK3c1zRi10ohZ4moCLpkFbHLNoQQOvSXOGiQIjIjhXxt2O9s9YcsYW6EgmOwrY2Qo6tOotM87V/tFmLoprpP+k3mLZv/2ViaoKIS9E2BA="),
            new Preencrypted("09876543210",
                    "h8NJlbICF5qXWWFHR+zV5XwD16Tmi5+lyIWOnxY7TmA+SnWsV8+2P0hMWwGB0n0DHOA/1sECgDaADlUz4lKf0x0fchjDlHar9kh8nu1/frYQPjKF7OuFO8giAMvJs2XHJMtjvpxkbOaHoEBOVRu481qL7RWEZTQl1zjoaYRpHaZ1GuI7hMy7XTJHSXB1FgRx4LYpd8XCs7vGsW5dO5rHFaA24buJOvTeST8y7fDqTa/ZrcwOqYJH3qrXhDRzcNHTm3VYvgwLh/15n2h0PzOK6pgNmqdRUGYnJgnhDqTm6cWzZf3L95ZJk18KrEprOgmWH4bOlx5wNOeOoeCVUz1V/tTo0NRfW2XEjvtFai/tCkImdBmv31wUbzvfeQpfqW7L19/gkOOsoi7vUHqHbAJDYjA+6qYUElrN+ShvCC4OO3Y2OHhjMARdjjPx2wbx8REAoWz2aKMK4ABO/XzjgVT8g95nBPzqYv2LZ/Do9xp1dJGERv4+62Vhi5GQ0fWWUC84lQ1rS3ZXfnj7hLVS+5na9VPwshjNi7MtI4co/qcMjEffkUrksPhoUdwGY1vkOchlIiJYZRvC36HkxB0DWGO/FJCXzDyy4bO+yFHptYq+80WwPn5ausZP2BbpoUYeIZQGwE6NemFB9ce1dFrq62yRKQ+tbeNYh5+8av8Z1q+bNxM=",
                    "aQ+leqJIu69uj7IPPWcyUwGUu7tlfw1mbWY2dBF0xNoUYbGjygLasmHlj0RmvhjaILc91sMA53DkeGgsDQpNPTC/8a5AUhwlkk0/mYXAK9H6HCGStiogCh+ifD5Jl9nZEI5fLYQUbxI9ofA+anCikakUIZp/fQg0w+zh5XvV66GnfYqkBIEYQfwb8+3jwWqo+0dxApFTrpRv5OIqpltdGUG8HQU5zX86D5mncQifmwRPP7oOHfNSzpNyZ6gT40dERvAH96aBcWnhiNNfkk9wLCxFi0xvQU4/Dx/g1mfwYLfc6EujT1Wa4DjuBdAIX678+WTqdOtpA4mLvdL59gIwmp3DGCC23I76/LHQh67hrH/EuiU5KRmrZToaMDig0uMF9n8yCMgZtIsr8Ob/+9XXfZKs/xZdrhtMCBGLCr7LlGMCJvPnXk18fbx8/vKQ++bc+oNb1cbn2YMBXVz0nUenHHA70SfrRoQA2a4InmKMqO94kl8sQsHqkL28V93UTPFSc5CQCUaUMOTEmd2RArbIhiesozL8dW7p6o5fkAT9f7KmJsdIpme62fJUFkZOrq73P06EsA9yh7aFihNdRf2nC0MiT/QIsyZqhxWbcU+qo4sloFrjNHv1Y5WEsBwny7RJkyQprIeYuLLbNeKokfKPxORwyhl2l1oamPNz0LdM0y4=")
    ));

    /** This is for demonstration purposes only. DO NOT USE IN PRODUCTION! */
    private static final String PASSWORD = "changeit";

    private static final String KEY_ALIAS = "example";

    /**
     * @param args Additional encrypted values to try decrypting
     */
    public static void main(String[] args) throws Exception {
        Decryptor decryptor = new Decryptor(retrievePrivateKey());
        encryptAndThenDecrypt(decryptor);
        validatePreencryptedValues(decryptor);
        decryptAdditional(args, decryptor);
        System.out.println("All encryption and decryption operations passed.");
    }

    private static void validatePreencryptedValues(Decryptor decryptor) throws Exception {
        for (Preencrypted preencryptedValue : PREENCRYPTED_VALUES) {
            String decryptedFromMgf1Sha1 = decryptor.decrypt(preencryptedValue.mgf1Sha1);
            assertEquals(preencryptedValue.decrypted, decryptedFromMgf1Sha1);
            String decryptedFromMgf1Sha256 = decryptor.decrypt(preencryptedValue.mgf1Sha256);
            assertEquals(preencryptedValue.decrypted, decryptedFromMgf1Sha256);
        }
    }

    private static void decryptAdditional(String[] additionalEncryptedValues, Decryptor decryptor) throws Exception {
        for (String encryptedValue : additionalEncryptedValues) {
            String decrypted = decryptor.decrypt(encryptedValue);
            String formatted = String.format(Locale.US, "The encrypted value:\n%s\ndecrypts to:\n%s\n", encryptedValue, decrypted);
            System.out.println(formatted);
        }
    }

    private static void encryptAndThenDecrypt(Decryptor decryptor) throws Exception {
        PublicKey publicKey = retrievePublicKey();
        Encryptor encryptor = new Encryptor(publicKey);
        String unencrypted = "12345678901234";
        String encrypted = encryptor.encrypt(unencrypted);
        String decrypted = decryptor.decrypt(encrypted);
        assertEquals(unencrypted, decrypted);
    }

    /**
     * The RSA public key is derived from the X.509 certificate, stored in PEM format.
     */
    private static PublicKey retrievePublicKey() throws CertificateException, IOException {
        try (InputStream inputStream = Examples.class.getResourceAsStream("/cert.crt")) {
            final CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            Certificate certificate = certificateFactory.generateCertificate(inputStream);
            return certificate.getPublicKey();
        }
    }

    private static PrivateKey retrievePrivateKey() throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException, UnrecoverableKeyException {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        char[] password = PASSWORD.toCharArray();
        try (InputStream inputStream = Examples.class.getResourceAsStream("/keystore.p12")) {
            keyStore.load(inputStream, password);
            return  (PrivateKey) keyStore.getKey(KEY_ALIAS, password);
        }
    }

    private static void assertEquals(String expected, String actual) {
        if (!expected.equals(actual)) {
            String errorMessage = String.format(Locale.US, "Expected %s but was %s", expected, actual);
            throw new IllegalStateException(errorMessage);
        }
    }

    private static class Preencrypted {
        final String decrypted;
        final String mgf1Sha1;
        final String mgf1Sha256;

        Preencrypted(String decrypted, String mgf1Sha1, String mgfSha256) {
            this.decrypted = decrypted;
            this.mgf1Sha1 = mgf1Sha1;
            this.mgf1Sha256 = mgfSha256;
        }
    }
}
