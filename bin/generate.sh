#!/bin/sh

set -e

base_directory="$(dirname "$0")/.."

resources="${base_directory}/src/main/resources"

private_key="${resources}/private.key"

x509_certificate="${resources}/cert.crt"

public_key="${resources}/public.crt"

pkcs12_keystore="${resources}/keystore.p12"

# DON'T DO THIS IN PRODUCTION!
insecure_password='changeit'

# 3,650 days is for integration testing only. Normally, keys are rotated more frequently!
echo 'Generating RSA private key and X.509 certificate...'
openssl req -x509 -sha256 -days 3650 -newkey rsa:2048 -passout pass:"${insecure_password}" \
  -subj '/C=US/ST=IL/L=Chicago/O=Example/CN=rewardsnetwork.example.com' \
  -keyout "${private_key}" -out "${x509_certificate}"

# In Production, private keys and key stores should be stored and transmitted securely!
echo
echo 'Creating public key from private key...'
openssl rsa -in "${private_key}" -passin pass:"${insecure_password}" -pubout -out "${public_key}"

# In Production, private keys and key stores should be stored and transmitted securely!
echo
echo 'Generating PKCS12 key store...'
openssl pkcs12 -export \
  -in "${x509_certificate}" -inkey "${private_key}" -passin pass:"${insecure_password}" \
  -out "${pkcs12_keystore}" -passout pass:"${insecure_password}" -name example

git add "${resources}/"
